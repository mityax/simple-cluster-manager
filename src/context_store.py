import json
import logging
import os.path
from typing import Any, Dict, Optional, Type, Union

from src.cli.utils import DEFAULT_INPUT_METHOD, get_current_cli
from src.providers.base.base_provider import BaseProvider

Context = Dict[str, Any]

base_dir = os.path.expanduser('~/.simple_cluster_manager')
os.makedirs(base_dir, exist_ok=True)

FILE = os.path.join(base_dir, 'contexts.json')

__contexts: Optional[Dict[str, Context]] = None
__current_context: Optional[str] = None


def get_all_contexts(force_reload: bool = False) -> Dict[str, Context]:
    global __contexts, __current_context

    if force_reload or __contexts is None:
        if not os.path.isfile(FILE):
            return {}
        with open(FILE, "r") as f:
            data = json.load(f)
        __contexts = data.get('contexts', {})
        __current_context = data.get('current', None)
    return dict(__contexts)


def add_context(name: str, provider: Union[BaseProvider, Type[BaseProvider]], context: Context):
    global __contexts
    __contexts = get_all_contexts()
    __contexts[name] = {
        '_provider': provider.slug,
        '_name': name,
        **context
    }
    save_contexts()


def remove_context(name: str):
    get_all_contexts()  # ensure that the contexts are loaded before removing
    del __contexts[name]
    save_contexts()


def save_contexts():
    with open(FILE, 'w+') as f:
        json.dump({
            'current': __current_context,
            'contexts': __contexts
        }, f)


def get_current_context():
    global __current_context

    if get_current_cli() is not None:
        if __current_context is None:
            logging.info('There is currently no default context selected.')
            __current_context = DEFAULT_INPUT_METHOD('context_name', 'Enter name of new default context')

    return get_all_contexts().get(__current_context)


def set_current_context(name: str):
    global __current_context

    __current_context = name
    save_contexts()
