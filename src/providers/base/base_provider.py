import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Dict, Any, List, Tuple, Iterable, Union, Optional

from src.providers.base.base_node import BaseNode


class BaseProvider(ABC):
    @property
    @abstractmethod
    def slug(self) -> str:
        """
        The slug of this provider
        """

    @staticmethod
    @abstractmethod
    def create_new_context(*args, **kwargs) -> Dict[str, Any]:
        """
        Create a new context for this cloud provider. When data input is needed, the given `input_method` callback is
        used. The callback receives two string arguments, first is the machine-readable identifier of the requested
        data (e.g. for automation) and second is the human-readable name.

        :return: The newly created context.
        """

    @classmethod
    @abstractmethod
    def from_context(cls, context: Dict[str, Any]) -> 'BaseProvider':
        """
        Create an instance of this cloud provider from the given context.

        :param context: The context as returned by `create_new_context`
        """

    @abstractmethod
    def get_nodes(self) -> List[BaseNode]:
        """
        Get all nodes managed by this provider.
        """

    @abstractmethod
    def run_command(
        self,
        node: BaseNode,
        command: List[str],
        in_cli: bool = True,
    ) -> Tuple[bytes, int]:
        """
        Run a command on the node (usually via a protocol like SSH).

        :param node: The node to run the command on
        :param command: The command to run, along with it's arguments
        :param in_cli: Whether to print the output of the command to the console or not
        :return: The stdout and return code of the executed command after it is completed.
        """

    def apply_config(self, config: Dict[str, Any]):
        """
        Apply the given cluster config to the cluster.

        :param config: The parsed cluster config. See the example-cluster-config.yaml file in the root of this project.
        """

        nodes = self.get_nodes()

        for node_type in config['node_types']:
            self.rebuild_nodes(
                old_nodes=[n for n in nodes if n.type == node_type['name']],
                node_type=node_type['name'],
                count=node_type['count'],
                strategy=node_type.get('rebuild_strategy', 'sequential'),
                node_config=node_type['node_config'],
                workloads=node_type['workloads'],
                script_config=node_type.get('run'),
            )

    def rebuild_nodes(
        self,
        old_nodes: List[BaseNode],
        node_type: str,
        count: int,
        strategy: str,
        node_config: Dict[str, Any],
        workloads: Dict[str, Any],
        script_config: Optional[List[Dict[str, Any]]] = None,
    ):
        """
        Rebuild the nodes of the given type. Any nodes in "old_nodes" will be deleted during this operation.
        """
        if strategy == 'sequential':
            self.rebuild_sequentially(count=count, node_config=node_config, node_type=node_type,
                                      old_nodes=old_nodes, workloads=workloads, script_config=script_config)
        else:
            raise NotImplementedError(f'Rebuild strategy "{strategy}" is not implemented.')

    def rebuild_sequentially(
        self,
        old_nodes: List[BaseNode],
        node_type: str,
        count: int,
        node_config: Dict[str, Any],
        workloads: Dict[str, Any],
        script_config: Optional[List[Dict[str, Any]]] = None,
    ):
        """
        Rebuild the cluster node by node, always first deleting a node and then creating a new one.
        """
        old_nodes = sorted(old_nodes, key=lambda n: n.name)
        names = list(self.generate_free_node_names(max(count - len(old_nodes), 0), node_type, old_nodes))
        for i in range(count):
            if old_nodes:  # replace old nodes, if any
                old_node = old_nodes.pop()
                logging.debug(f"Replacing node {old_node.name}...")
                self.delete_node(old_node)
                name = old_node.name  # re-use the name of the deleted node name to avoid conflicts
            else:  # if no old nodes are left, create a new one (up scaling)
                name = names.pop()

            logging.debug(f"Creating new node of type {node_type} ({i+1} of {count})...")
            node = self.create_node(type=node_type, name=name, config=node_config, workloads=workloads,
                                    scripts=get_scripts_for_node(script_config, i, count))

        while old_nodes:  # clear up remaining old nodes (down scaling)
            old_node = old_nodes.pop()
            logging.debug(f"Deleting no longer needed node {old_node.name}...")
            self.delete_node(old_node)

    @abstractmethod
    def delete_node(self, node: BaseNode):
        """
        Stop the given node and remove it from the cluster.
        :param node: The node to remove.
        """

    @abstractmethod
    def create_node(self, type: str, name: str, config: Dict[str, Any], workloads: Dict[str, Any],
                    script: Optional[List[str]] = None, wait_until_finished: bool = True):
        """
        Add a new node to the cluster using the given node config and launch the given workloads on it.
        :param type: The type of the node, taken from the cluster config file.
        :param name: The name of this node in the cluster.
        :param config: The provider-specific configuration for this node.
        :param workloads: The workloads (containers) to run on this node.
        :param script: A list of commands to run on the node after initializing.
        :param wait_until_finished: Whether to wait until the node has been completely processed, i.e. is up and running
        :return:
        """

    def generate_free_node_names(self, count: int, node_type: str, old_nodes: List[BaseNode]) -> Iterable[str]:
        """
        Generate names for `count` new nodes in the cluster, skipping those which already exist.
        :param count: The number of names to generate
        :param node_type: The type of the node
        :param old_nodes: The existing nodes in this cluster
        """
        old_node_names = [n.name for n in old_nodes]
        i = 0
        generated_names = 0
        while generated_names < count:
            if (name := self.generate_node_name(node_type, i + 1)) not in old_node_names:
                yield name
                generated_names += 1
            i += 1

    def generate_node_name(self, node_type: str, unique_identifier: Any) -> str:
        """
        Generate a name for a node from the given information.
        """
        return f'{node_type}-{unique_identifier}'

    def run_health_checks(self, node: BaseNode, workloads: List[Dict[str, Any]], ignore_error_on_required: bool = False) -> Iterable['HealthCheckResult']:
        """
        Run the health checks for the given workloads on the given node.
        :param node: The node to run health checks on
        :param workloads: The workloads to run health checks for
        :param ignore_error_on_required: Whether to ignore failed health
               checks that are marked as required or throw a RuntimeException (default)
        """

        for workload in workloads:
            res = self.run_health_check(node, workload)
            if not ignore_error_on_required and not res.success and workload['health_check']['required']:
                raise RuntimeError(f'Required health check failed for workload {workload["name"]} on node {node.name}')
            yield res

    def run_health_check(self, node: BaseNode, workload: Dict[str, Any]) -> 'HealthCheckResult':
        """
        Run a specific health check on a specific node.
        :param node: The node to run the health check on
        :param workload: The workload to run the health check for
        """
        check = workload['health_check']
        protocol = "https" if check.get("use_https", False) else "http"

        output, exit_code = self.run_command(node, [
            'curl', '-s',
            '-w', '"%{http_code}"',
            f'{protocol}://{check.get("host", "localhost")}:{check.get("port", 80)}/{check.get("path", "").lstrip("/")}'
        ], in_cli=False)

        logging.debug(f"Health Check Output: {output} and exit code {exit_code}")

        if exit_code != 0:  # usually curl error for host unreachable (exit code 7), but also catch others
            return HealthCheckResult(
                success=False,
                workload=workload,
                node=node,
                response=None,
                status_code=None
            )

        r = output.decode().rsplit('\n', 1)
        body, status_code = r if len(r) == 2 else (None, r[0])

        return HealthCheckResult(
            success=status_code.strip() in [s.strip() for s in check['status_code'].split(',')],
            workload=workload,
            node=node,
            response=body,
            status_code=int(status_code.strip())
        )



@dataclass
class HealthCheckResult:
    success: bool
    response: Union[bytes, None]
    status_code: Union[int, None]
    workload: Dict[str, Any]
    node: BaseNode


# Utils:

def get_scripts_for_node(script_config: Optional[List[Dict[str, Any]]], index: int, count: int) -> Tuple[List[str], List[str]]:
    before = []
    after = []

    if script_config:
        for script in script_config:
            on = script.get('on', script.get(True, 'all'))  # the yaml lib replaces "on" as a key with `True`...
            target = before if script.get('before', False) else after
            if (
                    on == 'all'
                or (on == 'first' and index == 0)
                or (on == 'last'  and index == count - 1)  # noqa
            ):
                if 'workload' in script:
                    boundary = 'BOUNDARY_4732640f27340f8732d0721hd_BOUNDARY'
                    target.append(f"docker exec -i $(docker ps -qf 'name={script['workload']}') sh << '{boundary}'\n  " \
                                  + '\n  '.join(script['script']) \
                                  + f'\n{boundary}')
                else:
                    target += script['script']

    return before, after
