from libs.hcloud.servers.client import BoundServer
from src.providers.base.base_node import BaseNode
from src.providers.hetzner_cloud.constants import LABEL_NODE_TYPE


class HetznerNode(BaseNode):
    def __init__(self, name: str, server: BoundServer):
        self.__name = name
        self.server = server

    @property
    def name(self) -> str:
        return self.__name

    @property
    def type(self) -> str:
        return self.server.labels[LABEL_NODE_TYPE]

