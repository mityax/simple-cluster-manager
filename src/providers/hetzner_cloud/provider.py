import base64
import logging
import os.path
import re
import shlex
import subprocess
import time
from typing import Dict, Any, List, Optional, Tuple, Union

import yaml

from libs.hcloud import hcloud
from src.cli.utils import cli
from src.providers.base.base_node import BaseNode
from src.providers.base.base_provider import BaseProvider
from src.providers.hetzner_cloud.constants import LABEL_SSH_USERNAME, LABEL_CLUSTER_NAME, LABEL_NODE_TYPE, LABEL_GROUP, \
    LABEL_SSH_KEY_GROUPS
from src.providers.hetzner_cloud.node import HetznerNode
from src.utils.ping import wait_until_any_port_open
from src.utils.utils import ShellCommand, v

with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'templates/cloud_init_docker.yaml')) as f:
    CLOUD_INIT_DATA = f.read()


def generate_cloud_init(script: List[str]) -> str:
    parsed = yaml.safe_load(CLOUD_INIT_DATA)
    parsed['runcmd'] += script
    return '#cloud-config\n' + yaml.dump(parsed)


class HetznerCloudProvider(BaseProvider):
    slug = 'hetzner'

    def __init__(self, cluster_name: str, token: str):
        self.token = token
        self.cluster_name = cluster_name
        self.client = hcloud.Client(self.token)

    @staticmethod
    @cli(
        token='Hetzner Cloud Token'
    )
    def create_new_context(token: str) -> Dict[str, Any]:
        client = hcloud.Client(token=token)
        logging.info("Testing connection to Hetzner Cloud Project...")
        client.servers.get_all()
        logging.info("Connection successful.")

        return {
            'token': token
        }

    @classmethod
    def from_context(cls, context: Dict[str, Any]) -> 'HetznerCloudProvider':
        return HetznerCloudProvider(
            cluster_name=slugify(context['_name']),  # cluster name is derived from the context's name
            token=context['token']
        )

    def get_nodes(self) -> List[HetznerNode]:
        servers = list(
            HetznerNode(
                name=self.__external_node_name(server.name),
                server=server
            )
            for server in self.client.servers.get_all(label_selector=f'{LABEL_CLUSTER_NAME}=={self.cluster_name}')
        )
        return servers

    def run_command(
            self,
            node: BaseNode,
            command: List[str],
            timeout: Optional[int] = 120,
            in_cli: bool = True
    ) -> Tuple[bytes, int]:
        assert isinstance(node, HetznerNode)

        command = self.__get_ssh_command_for_node(command, node, timeout)
        logging.debug(' '.join(command))
        res = subprocess.run(' '.join(command), shell=True,
                             stderr=None if in_cli else subprocess.PIPE,
                             stdout=None if in_cli else subprocess.PIPE)
        return res.stdout, res.returncode

    def __get_ssh_command_for_node(self, command: List[str], node: HetznerNode, timeout: Optional[float] = None) -> \
    List[str]:
        ip, _ = wait_until_any_port_open([
            (ip, 22) for ip in self.__get_node_ips(node)
        ], timeout=timeout)
        logging.debug(f'Trying to reach server through IP {ip}...')
        options = [
            '-o', 'StrictHostKeyChecking=no'
        ]
        if timeout is not None:
            options += ['-o', f'ConnectTimeout={timeout}']
        c = ['ssh', *options, f'{node.server.labels[LABEL_SSH_USERNAME]}@{ip}', *command]
        return c

    def __get_node_ips(self, node: HetznerNode):
        return [
            ip.ip for ip in [
                *[n for n in node.server.private_net],
                node.server.public_net.ipv4 or node.server.public_net.ipv6
            ] if ip is not None
        ]

    def create_node(self, type: str, name: str, config: Dict[str, Any], workloads: List[Dict[str, Any]],
                    scripts: Tuple[List[str], List[str]] = None, wait_until_finished: bool = True) -> HetznerNode:
        placement_group = self.client.placement_groups.get_by_name(self.cluster_name)

        if placement_group is None:
            placement_group = self.client.placement_groups.create(
                name=self.cluster_name,
                type='spread',
                labels={
                    LABEL_CLUSTER_NAME: self.cluster_name,
                }
            ).placement_group

        cloud_init: str = generate_cloud_init(script=self.__generate_cloud_init_script(
            scripts=scripts,
            workloads=workloads
        ))

        logging.debug(f"cloud-init config:\n{cloud_init}")

        server_response = self.client.servers.create(
            name=self.__internal_node_name(name),
            server_type=self.client.server_types.get_by_name(config['server']),
            image=self.client.images.get_by_name(config.get('os', config.get('operating_system'))),
            ssh_keys=self.client.ssh_keys.get_all(  # all ssh keys that should be added to this server
                label_selector=f'{LABEL_GROUP} in ({",".join(config["ssh_key_groups"])})' if 'ssh_key_groups' in config else ''
            ),
            networks=self.client.networks.get_all(label_selector=f'{LABEL_CLUSTER_NAME}=={self.cluster_name}'),
            labels={
                LABEL_CLUSTER_NAME: self.cluster_name,
                LABEL_NODE_TYPE: type,
                LABEL_SSH_USERNAME: 'root',
                LABEL_SSH_KEY_GROUPS: ','.join(config.get('ssh_key_groups', [])),
                **config.get('labels', {})
            },
            location=self.client.locations.get_by_name(config['location']),
            placement_group=placement_group,
            user_data=cloud_init
        )

        node = HetznerNode(
            name=name,
            server=server_response.server
        )

        if wait_until_finished:
            logging.debug("Waiting until the server is provisioned...")
            server_response.action.wait_until_finished()
            self.__ensure_running(node, workloads)

        return node

    def delete_node(self, node: HetznerNode):
        assert isinstance(node, HetznerNode)

        self.client.servers.delete(node.server)

    def __internal_node_name(self, name):
        return f'{self.cluster_name}-{name}'

    def __external_node_name(self, internal_name):
        return internal_name[len(self.cluster_name) + 1:]

    def __generate_cloud_init_script(self, scripts: Tuple[List[str], List[str]],
                                     workloads: List[Dict[str, Any]]) -> List[str]:
        res: List[Union[str, ShellCommand]] = scripts[0] if scripts else []

        for workload in workloads:
            image = workload["image"]

            if 'username' in image and 'password' in image and 'registry' in image:  # login
                res.append(
                    ShellCommand('docker login')
                    .kwarg('-u', image['username'])
                    .kwarg('-p', image['password'])
                    .arg(image['registry'])
                    .build()
                )

            image_url = f'{image.get("registry", "")}/{image["name"]}'.lstrip('/')

            run_command = ShellCommand('docker run')\
                .flag('-d')\
                .kwarg('--name', workload["name"])\
                .kwarg('--restart', 'unless-stopped')

            if 'ports' in workload:
                run_command.kwarg('--publish', workload['ports'])
            if 'volumes' in workload:
                run_command.kwarg('--mount', workload['volumes'])
            if 'env' in workload:
                # save env variables to file:
                env_file = f'{workload["name"]}.env'
                env_file_contents = base64.b64encode(
                    '\n'.join(f'{k}=' + str(v).replace('\n', '\\n') for k, v in workload['env'].items()).encode()
                ).decode()
                res.append(
                    ShellCommand('echo').arg(env_file_contents) | ShellCommand('base64 -d >').arg(env_file)
                )
                run_command.kwarg('--env-file', env_file)
            if 'entrypoint' in workload:
                run_command.kwarg('--entrypoint', workload['entrypoint'])

            run_command.arg(image_url)

            if 'command' in workload:
                run_command.arg(workload['command'], quote=False)

            res.append(run_command)

        if len(scripts) == 2:
            res += scripts[1]
        return [str(x) for x in res]  # convert `ShellCommand` instances to string

    def __ensure_running(self, node: HetznerNode, workloads: List[Dict[str, Any]]):
        required_health_checks = [w for w in workloads if w.get('health_check', {}).get('required')]
        succeeded = []
        while len(succeeded) < len(required_health_checks):
            # Run all required health checks
            results = self.run_health_checks(
                node,
                [c for c in required_health_checks if c not in succeeded],
                ignore_error_on_required=True
            )
            succeeded = [r.workload for r in results if r.success]
            time.sleep(1)


def slugify(name: str):
    return re.sub('[^a-zA-Z0-9_-]', '-', name.lower())
