
# Labels allow us to store meta data with individual nodes
_LABEL_PREFIX = 'scm/'

LABEL_CLUSTER_NAME = f'{_LABEL_PREFIX}cluster-name'
LABEL_NODE_TYPE = f'{_LABEL_PREFIX}node-type'
LABEL_SSH_USERNAME = f'{_LABEL_PREFIX}ssh-username'
LABEL_GROUP = f'{_LABEL_PREFIX}group'
LABEL_SSH_KEY_GROUPS=f'{_LABEL_PREFIX}ssh-key-groups'
