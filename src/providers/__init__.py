import logging
from typing import List, Type

from src.context_store import Context
from src.providers.base.base_provider import BaseProvider
from src.providers.hetzner_cloud.provider import HetznerCloudProvider


ALL_PROVIDERS: List[Type[BaseProvider]] = [
    HetznerCloudProvider
]


def create_provider(context: Context) -> BaseProvider:
    try:
        return next(filter(lambda p: p.slug == context['_provider'], ALL_PROVIDERS)).from_context(context)
    except StopIteration:
        logging.debug(f'Failed to find provider for context: {context}')
        raise ValueError(f'Unknown provider type: {context["_provider"]}')
