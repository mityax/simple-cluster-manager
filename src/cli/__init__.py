import argparse
import logging
from typing import Optional, Sequence

from src.providers import ALL_PROVIDERS
from .utils import Cli
from .actions import context_actions, nodes_actions, apply_actions


def run_from_commandline(raw_args: Optional[Sequence[str]] = None):
    parser = argparse.ArgumentParser(
        prog='__main__.py'
    )

    # General arguments:
    parser.add_argument('--verbose', '-v', action='store_true', default=False)
    subparsers = parser.add_subparsers()

    # 'context' subcommand:
    context_actions.add_parser_to(subparsers)

    # 'nodes' subcommand:
    nodes_actions.add_parser_to(subparsers)

    # 'apply' subcommand:
    apply_actions.add_parser_to(subparsers)

    # Parse arguments and handle them
    arguments = parser.parse_args(raw_args)

    _configure_logger(verbose=arguments.verbose)

    if hasattr(arguments, 'func') and arguments.func:
        arguments.func(arguments)
    else:
        parser.print_usage()


def _configure_logger(verbose: bool = False):
    config = dict(
        format='[%(levelname)s] %(message)s'
    )

    if verbose:
        logging.basicConfig(level=logging.DEBUG, **config)
    else:
        logging.basicConfig(level=logging.INFO, **config)
