import argparse
from typing import Type

from src import context_store
from src.cli import Cli
from src.providers import ALL_PROVIDERS
from src.providers.base.base_provider import BaseProvider


def create(args: argparse.Namespace):
    provider: Type[BaseProvider] = next(filter(lambda a: a.slug == args.provider.lower(), ALL_PROVIDERS))

    arguments = {}
    if args.token:
        arguments['token'] = args.token

    with Cli():
        context = provider.create_new_context(**arguments)

    context_store.add_context(args.name, provider, context)
    print("Context added successfully.")


def list(args: argparse.Namespace):
    contexts = context_store.get_all_contexts()

    if contexts:
        print("Available contexts are:")
        for name, context in contexts.items():
            curr = context_store.get_current_context()['_name'] == name
            print(f' - {name} ({context["_provider"]})' + (' - currently used' if curr else ''))
    else:
        print('There are no contexts. Add a context using the "context create" command.')


def remove(args: argparse.Namespace):
    contexts = context_store.get_all_contexts()
    if args.name not in contexts:
        print(f'Context "{args.name}" does not exist.')
    else:
        context_store.remove_context(args.name)
        print(f'Context "{args.name}" with provider "{contexts[args.name]["_provider"]}" has been removed.')


def use(args: argparse.Namespace):
    contexts = context_store.get_all_contexts()
    if args.name not in contexts:
        print(f'Context "{args.name}" does not exist.')
    else:
        context_store.set_current_context(args.name)
        print(f'Set context "{args.name}" with provider "{contexts[args.name]["_provider"]}" as default context.')


def add_parser_to(subparsers):
    parser = subparsers.add_parser('context', aliases=['ctx'])
    context_subparsers = parser.add_subparsers()

    context_create_parser = context_subparsers.add_parser('create')
    context_create_parser.add_argument('provider', choices=[provider.slug for provider in ALL_PROVIDERS])
    context_create_parser.add_argument('--name', '-n', required=True)
    context_create_parser.add_argument('--token', '-t')
    context_create_parser.set_defaults(func=create)

    context_list_parser = context_subparsers.add_parser('list')
    context_list_parser.set_defaults(func=list)

    context_remove_parser = context_subparsers.add_parser('remove')
    context_remove_parser.add_argument('name')
    context_remove_parser.set_defaults(func=remove)

    context_use_parser = context_subparsers.add_parser('use')
    context_use_parser.add_argument('name')
    context_use_parser.set_defaults(func=use)

    return parser
