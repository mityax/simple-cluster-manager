import argparse
import sys
from typing import List

from src import context_store
from src.providers import create_provider
from src.providers.base.base_node import BaseNode


def run(args: argparse.Namespace):
    command = ' '.join(args.command)
    provider = create_provider(context_store.get_current_context())
    nodes = provider.get_nodes()

    if not nodes:
        print(f'The cluster for context {context_store.get_current_context()["_name"]} has no nodes '
              f'to run a command on.', file=sys.stderr)
        sys.exit(1)

    run_on: List[BaseNode] = nodes

    if args.node:
        run_on = list(filter(lambda n: n.name == args.node, nodes))
        if not run_on:
            print(f'The cluster for context {context_store.get_current_context()["_name"]} has no node '
                  f'with name {args.node}.', file=sys.stderr)
            sys.exit(1)
    elif not args.all:
        run_on = [run_on[0]]

    print(f'Running command "{command}" on {len(run_on)} node(s).')
    for i, node in enumerate(run_on, 1):
        print(f'Running on node #{i} {node.name}:')
        stdout, code = provider.run_command(node, args.command)
        print(code)


def list_(args: argparse.Namespace):
    context = context_store.get_current_context()
    provider = create_provider(context)
    nodes = provider.get_nodes()

    if nodes:
        print(f'Cluster for context "{context["_name"]}" has {len(nodes)} node(s):')
        for node in nodes:
            print(f' - {node.name} ({node.type})')
    else:
        print(f'The cluster for context "{context["_name"]}" has no nodes.')


def remove(args: argparse.Namespace):
    pass


def add_parser_to(subparsers):
    parser = subparsers.add_parser('nodes')
    context_subparsers = parser.add_subparsers()

    context_create_parser = context_subparsers.add_parser('run', aliases=['exec'])
    context_create_parser.add_argument('--all', '-a', action='store_true')
    context_create_parser.add_argument('--node', '-n')
    context_create_parser.add_argument('command', nargs=argparse.REMAINDER)
    context_create_parser.set_defaults(func=run)

    context_list_parser = context_subparsers.add_parser('list')
    context_list_parser.set_defaults(func=list_)

    context_rebuild_parser = context_subparsers.add_parser('rebuild')
    context_rebuild_parser.add_argument('node')
    context_rebuild_parser.set_defaults(func=remove)

    #context_use_parser = context_subparsers.add_parser('use')
    #context_use_parser.add_argument('name')
    #context_use_parser.set_defaults(func=use)

    return parser
