import argparse
import sys

from src import context_store
from src.providers import create_provider
from src.utils.cluster_config import parse_cluster_config


def apply(args: argparse.Namespace):
    config = parse_cluster_config(args.config_file)
    context = context_store.get_current_context()

    if context['_provider'] != config['cluster']['provider']:
        print(f'The current context ({context["_name"]}) uses the provider {context["_provider"]} but the cluster '
              f'config file is created for provider {config["cluster"]["provider"]}.', file=sys.stderr)
        sys.exit(1)

    provider = create_provider(context)

    provider.apply_config(config['cluster'])


def add_parser_to(subparsers):
    parser: argparse.ArgumentParser = subparsers.add_parser('apply')

    parser.add_argument('--config-file', '-f', default='cluster.yaml')
    parser.set_defaults(func=apply)

    return parser
