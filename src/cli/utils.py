from typing import Callable, Optional

DEFAULT_INPUT_METHOD = lambda identifier, name: input(f'{name}: ')  # noqa


class Cli:
    def __init__(self, input_method: Callable[[str, str], str] = DEFAULT_INPUT_METHOD):
        self.input_method = input_method

    def __enter__(self):
        global _current_cli
        _current_cli = self

    def __exit__(self, exc_type, exc_val, exc_tb):
        global _current_cli
        _current_cli = None


_current_cli: Optional[Cli] = None


def cli(**properties):
    def wrapper(func):
        def call(*args, **kwargs):
            if not _current_cli:
                return func(*args, **kwargs)

            res = {
                k: _current_cli.input_method(k, v) for k, v in properties.items() if k not in kwargs
            }
            res.update(kwargs)
            return func(*args, **res)
        return call

    return wrapper


def get_current_cli() -> Optional[Cli]:
    """
    Returns the current `Cli` object, if in cli mode, `None` otherwise.
    """
    return _current_cli
