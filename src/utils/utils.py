import shlex
from typing import Optional, Union, Iterable, Any


class ShellCommand:
    def __init__(self, parts: Union[Iterable[Union[str, 'ShellCommand']], str, None] = None):
        if isinstance(parts, str):
            self.__parts = [parts]
        elif parts:
            self.__parts = list(parts)
        else:
            self.__parts = []

    def flag(self, flag: Union[str, Iterable[str]], condition: Any = True) -> 'ShellCommand':
        """
        Add a flag like "-d" or "--verbose" to the command.

        The flag will not be escaped. The optional `condition` argument can be
        used to easily add a flag depending on any condition logic, for example:

        ```
            show_all_files = input("Show all files? [y/n]") == 'y'
            command = ShellCommand('ls').flag('-a', show_all_files == 'y')
        ```

        If `flag` is an iterable, each element will be added as separate flag.
        """
        if condition:
            if isinstance(flag, str):
                flag = [flag]
            self.__parts.extend(flag)
        return self

    def kwarg(self, name: str, value: Union[str, Iterable[str]], quote: bool = True) -> 'ShellCommand':
        """
        Add a key-value pair to the command (e.g. "--name value").

        `value` is escaped if `quote` is truthy, `name` not. If value is an iterable,
        the kwarg will be added multiple times. For example:

        ```
            ports = ["80:80", "443:443"]
            command = ShellCommand('docker run').kwarg("--publish", ports)
            print(command)  # -> docker run --publish 80:80 --publish 443:443
        ```
        """
        if isinstance(value, str):
            value = [value]
        for part in value:
            self.__parts.append(name)
            self.__parts.append(shlex.quote(part) if quote else part)
        return self

    def arg(self, value: Union[str, Iterable[str]], quote: bool = True) -> 'ShellCommand':
        """
        Add a positioned argument to the command.

        The argument will be properly escaped if `quote` is truthy. If value is an
        iterable, each argument will be added (and escaped) separately. For example:

        ```
            directories = ["~/Documents", "~/Downloads", "~/My Pictures"]
            command = ShellCommand('ls').arg(directories)
            print(command)  # -> ls ~/Documents ~/Downloads "~/My Pictures"
        ```
        """
        if isinstance(value, str):
            value = [value]
        for part in value:
            self.__parts.append(shlex.quote(part) if quote else part)
        return self

    def pipe(self, other: 'ShellCommand'):
        return ShellCommand([self, '|', other])

    def build(self):
        parts = []
        for part in self.__parts:
            if isinstance(part, ShellCommand):
                parts.extend(part.__parts)
            else:
                parts.append(part)
        return ' '.join(parts)

    def __str__(self):
        return self.build()

    def __or__(self, other):
        return self.pipe(other)


def v(value: str) -> str:
    return shlex.quote(value)
