import logging
import socket
import time
from typing import Optional, List, Tuple


def tcp_ping(host: str, port: int, timeout: float = 1) -> bool:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)

    try:
        # Try to connect
        sock.connect((host, port))
        sock.shutdown(socket.SHUT_RD)
        return True
    # Connection Timed Out
    except socket.timeout:
        return False
    except OSError:
        return False


def wait_until_port_open(host: str, port: int, timeout: Optional[float] = None, interval: float = 0.5, connection_timeout: float = 1):
    start_time = time.time()

    while not timeout or time.time() - start_time < timeout:
        if tcp_ping(host, port, timeout=connection_timeout):
            return

        time.sleep(interval)

    raise TimeoutError(f"The time limit ({timeout}s) for waiting for host {host} to open port {port} was reached.")


def wait_until_any_port_open(targets: List[Tuple[str, int]], timeout: Optional[float] = None, interval: float = 0.5, connection_timeout: float = 1) -> Tuple[str, int]:
    start_time = time.time()

    i = 1
    while not timeout or time.time() - start_time < timeout:
        for host, port in targets:
            logging.debug(f"Trying to connect to {host} on port {port} (round #{i})")
            if tcp_ping(host, port, timeout=connection_timeout):
                return host, port

            time.sleep(interval)

        i += 1

    raise TimeoutError(f"The time limit ({timeout}s) for waiting for any port to open was reached.")
