import os
import string
from typing import Any, Mapping

import yaml
from jsonschema import Draft7Validator


def parse_cluster_config(config_file: str):
    with open(config_file, 'r') as stream:
        parsed = yaml.safe_load(stream)

    cluster_config_json_validator.validate(instance=parsed)

    if parsed.get('version') != 1:
        raise RuntimeError(f'Unsupported cluster config version: {parsed.get("version")}. Only version 1 is supported.')

    parsed = _populate_variables(parsed, os.environ)

    return parsed


def _populate_variables(data: Any, variables: Mapping[str, Any]) -> Any:
    """
    Populates the given variables recursively in the passed data, using `string.Template` ($NAME syntax)
    :param data: The parsed data to substitute variables in
    :param variables: The variables to substitute
    """
    if isinstance(data, dict):
        for k, v in data.items():
            data[k] = _populate_variables(v, variables)
    elif isinstance(data, list):
        for i, item in enumerate(data):
            data[i] = _populate_variables(item, variables)
    elif isinstance(data, str):
        try:
            data = string.Template(data).substitute(variables)
        except KeyError as e:
            raise KeyError(f"Missing environment variable: {e.args}")

    return data


cluster_config_json_validator = Draft7Validator(schema={
    'required': ['version', 'cluster'],
    'properties': {
        'version': {'type': 'number'},
        'definitions': {},
        'cluster': {
            'type': 'object',
            'required': ['provider', 'node_types'],
            'properties': {
                'provider': {'type': 'string'},
                'node_types': {
                    'type': 'array',
                    'items': {
                        'type': 'object',
                        'required': ['name', 'count', 'workloads', 'node_config'],
                        'properties': {
                            'name': {'type': 'string'},
                            'count': {'type': 'number'},
                            'node_config': {'type': 'object'},
                            'rebuild_strategy': {'const': 'sequential'},
                            'run': {
                                'type': 'array',
                                'items': {
                                    'type': 'object',
                                    'required': ['script'],
                                    'properties': {
                                        'on': {'anyOf': ['all', 'first', 'last'], 'type': 'string'},
                                        'workload': {'type': 'string'},
                                        'before': {'type': 'boolean'},
                                        'script': {
                                            'type': 'array',
                                            'items': {'type': 'string'}
                                        }
                                    }
                                }
                            },
                            'workloads': {
                                'type': 'array',
                                'items': {
                                    'type': 'object',
                                    'required': ['name', 'image'],
                                    'properties': {
                                        'name': {'type': 'string'},
                                        'image': {
                                            'type': 'object',
                                            'required': ['name'],
                                            'properties': {
                                                'name': {'type': 'string'},
                                                'registry': {'type': 'string'},
                                                'username': {'type': 'string'},
                                                'password': {'type': 'string'}
                                            }
                                        },
                                        'env': {
                                            'type': 'object',
                                            'additionalProperties': True
                                        },
                                        'ports': {
                                            'type': 'array',
                                            'items': {
                                                # TODO: Maybe add format validator for "\d+:\d+"
                                                'type': 'string',
                                            }
                                        },
                                        'volumes': {
                                            'type': 'array',
                                            'items': {'type': 'string'}
                                        },
                                        'health_check': {
                                            'type': 'object',
                                            'properties': {
                                                'port': {'type': 'number'},
                                                'path': {'type': 'string'},
                                                'status_code': {'type': 'string'},
                                                'required': {'type': 'boolean'}
                                            }
                                        },
                                        'entrypoint': {
                                            'type': 'string'
                                        },
                                        'command': {
                                            'type': 'string'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
})
