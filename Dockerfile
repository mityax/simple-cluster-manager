FROM python:3.9-slim-bullseye

ENV WORKDIR=/opt/simple-cluster-manager

WORKDIR $WORKDIR

COPY src src
COPY libs libs
COPY __main__.py .
COPY run.sh .
COPY requirements.txt .

RUN apt-get update
RUN apt-get install -y openssh-client
RUN pip install -r requirements.txt

RUN ln -s "$WORKDIR/run.sh" /bin/simple-cluster-manager
RUN ln -s "$WORKDIR/run.sh" /bin/scm

WORKDIR /root
