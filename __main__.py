#!/bin/env python3
from src.cli import run_from_commandline

if __name__ == '__main__':
    run_from_commandline()
