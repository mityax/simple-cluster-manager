SCRIPTPATH="$(dirname "$(readlink -f "$0")")"

PYTHONPATH="$PYTHONPATH:$SCRIPTPATH/libs:$SCRIPTPATH" python3 $SCRIPTPATH $@
